# Object-Oriented Programming Languages III final project #

## Author:
Jakub Goralewski

# About project:
My project is a simple farm simulator. User can buy animals, feed them and get products of them. All actions can be make by typing commands in console.

# Running:
Project has on external libs so after downloading source from bitbucket You have to simply open it by IDE like InteliJ IDEA or similar.
Project was made on Java 8

# Using:
Once You run the project You'll see possible commands on console. Always You can tyle "Help" to see them. Commands are intuitive