package com.jacob.animals;


import com.jacob.Products.Food;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.animals.excepions.SurfeitException;
import com.jacob.animals.interfaces.CanSpeak;

public class Dog extends Animal {

    public Dog() {
        System.out.println("Purchased Dog!");
    }

    public void speak() {
        System.out.println("How how!");
    }

    public void eat(Food food){
        try {
            this.addHunger(food.getMass()*6);
        } catch (SurfeitException e) {
            hunger = 100;
        }
    }

    public void starve() throws StarveToDeathException {
        hunger -= 3;
        if (hunger <= 0) {
            throw new StarveToDeathException("Dog starved to death!");
        }
    }


    @Override
    public String toString() {
        return "Dog: hunger=" + getHunger() + "%";
    }

}
