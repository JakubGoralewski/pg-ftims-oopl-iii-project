package com.jacob.animals;


import com.jacob.Products.Food;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.animals.excepions.SurfeitException;

public class Pig extends Animal {

    public Pig () {
        System.out.println("Purchased pig!");
    }

    public void eat(Food food){
        try {
            this.addHunger(food.getMass()*10);
        } catch (SurfeitException e) {
            hunger = 100;
        }
    }

    public void starve() throws StarveToDeathException {
        hunger -= 5;
        if (hunger <= 0) {
            throw new StarveToDeathException("Pig starved to death!");
        }
    }

    @Override
    public String toString() {
        return "Pig: hunger=" + getHunger() + "%";
    }
}
