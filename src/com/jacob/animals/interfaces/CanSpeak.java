package com.jacob.animals.interfaces;


public interface CanSpeak {
    void speak();
}
