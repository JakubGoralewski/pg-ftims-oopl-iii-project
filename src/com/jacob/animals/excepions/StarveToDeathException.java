package com.jacob.animals.excepions;


public class StarveToDeathException extends Exception {
    public StarveToDeathException(String message) {
        super(message);
    }
}
