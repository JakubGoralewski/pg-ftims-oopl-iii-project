package com.jacob.animals.excepions;


public class SurfeitException extends Exception {
    public SurfeitException(String message) {
        super(message);
    }
}
