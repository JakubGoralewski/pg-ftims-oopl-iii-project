package com.jacob.animals;

import com.jacob.Products.Fodder;
import com.jacob.Products.Food;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.animals.excepions.SurfeitException;
import com.jacob.animals.interfaces.CanSpeak;


public class Animal implements CanSpeak {
    protected int hunger;

    public Animal() {
        hunger = 100;
    }

    public void eat(Food food){
        try {
            this.addHunger(food.getMass()*20);
        } catch (SurfeitException e) {
            hunger = 100;
        }
    }

    public void starve() throws StarveToDeathException {
        hunger -= 10;
        if (hunger <= 0) {
            throw new StarveToDeathException("Animal starved to death!");
        }
    }

    protected void addHunger(int val) throws SurfeitException {
        hunger += val;
        if (hunger > 100) {
            throw new SurfeitException("Animal is surfeit!");
        }
    }

    public Food getProduct() {
        return new Fodder(0);
    }

    @Override
    public void speak(){}

    public int getHunger() {
        return hunger;
    }
}
