package com.jacob.animals;


import com.jacob.Products.Food;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.animals.excepions.SurfeitException;

public class Cow extends Animal {
    public Cow () {
        System.out.println("Purchased cow!");
    }

    public void eat(Food food){
        try {
            this.addHunger(food.getMass()*20);
        } catch (SurfeitException e) {
            hunger = 100;
        }
    }

    public void starve() throws StarveToDeathException {
        hunger -= 10;
        if (hunger <= 0) {
            throw new StarveToDeathException("Cow starved to death!");
        }
    }

    @Override
    public String toString() {
        return "Cow: hunger=" + getHunger() + "%";
    }
}
