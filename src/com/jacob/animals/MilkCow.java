package com.jacob.animals;


import com.jacob.Products.Food;
import com.jacob.Products.Milk;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.animals.excepions.SurfeitException;

public class MilkCow extends Cow {

    public MilkCow () {
        System.out.println("Purchased Milker!");
    }

    public void eat(Food food){
        try {
            this.addHunger(food.getMass()*30);
        } catch (SurfeitException e) {
            hunger = 100;
        }
    }

    public void starve() throws StarveToDeathException {
        hunger -= 15;
        if (hunger <= 0) {
            throw new StarveToDeathException("Milker starved to death!");
        }
    }

    public Food getProduct() {
        return new Milk(6);
    }

    @Override
    public String toString() {
        return "Milker: hunger=" + getHunger() + "%";
    }


}
