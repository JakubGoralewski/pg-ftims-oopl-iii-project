package com.jacob.farm;

import com.jacob.Products.Fodder;
import com.jacob.Products.Food;
import com.jacob.Products.Meat;
import com.jacob.Products.Milk;
import com.jacob.animals.*;
import com.jacob.animals.excepions.StarveToDeathException;
import com.jacob.utils.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm {
    private List<Animal> animals = new ArrayList<>();
    private Map<String, Command> commands = new HashMap<>();
    private Warehouse warehouse = new Warehouse();


    public Farm() {
        // quit command
        warehouse.storedFodder.add(new Fodder(20));
        Command quit = () -> System.exit(0);
        addCommand("Quit", quit);

        // help command
        Command help = () -> {
            System.out.println("Available commands:");
            for (String key : commands.keySet()){
                System.out.println("\t" + key);
            }
        };
        addCommand("Help", help);


        // buy pig command
        Command buyPig = () -> {
            Pig pig = new Pig();
            addAnimal(pig);
        };
        addCommand("BuyPig", buyPig);

        // buy cow command
        Command buyCow = () -> {
            Cow cow = new Cow();
            addAnimal(cow);
        };
        addCommand("BuyCow", buyCow);

        // buy dog command
        Command buyDog = () -> {
            Dog dog = new Dog();
            addAnimal(dog);
        };
        addCommand("BuyDog", buyDog);

        // buy dog command
        Command buyMilker = () -> {
            MilkCow milkCow = new MilkCow();
            addAnimal(milkCow);
        };
        addCommand("BuyMilker", buyMilker);

        // play with dog command
        Command play = () -> {
            boolean flag = false;
            for (Animal animal : animals) {
                if (animal instanceof Dog) {
                    flag = true;
                    animal.speak();
                }
            }
            if (!flag) {
                System.out.println("You have no dog!");
            }
        };
        addCommand("PlayWithDog", play);

        // get milk command
        Command milk = () -> {
            boolean flag = false;
            for (Animal animal : animals) {
                if (animal instanceof MilkCow) {
                    flag = true;
                    warehouse.storedMilk.add(new Milk(6));
                }
            }
            if(!flag){
                System.out.println("You have no milker!");
            }
        };
        addCommand("Milk", milk);

        // get milk command
        Command getProd = () -> {
            List<Animal> toDel = new ArrayList<>();
            for (Animal animal : animals) {
                if ((animal instanceof Cow) && !(animal instanceof MilkCow)) {
                    warehouse.storedMeat.add(new Meat(220));
                    toDel.add(animal);
                }
                if (animal instanceof Pig) {
                    warehouse.storedMeat.add(new Meat(80));
                    toDel.add(animal);
                }
            }
            for (Animal animal : toDel) {
                animals.remove(animal);
            }
        };
        addCommand("GetMeat", getProd);

        // feed animals command
        Command feed = () -> feedAnimals();
        addCommand("Feed", feed);

        // buy fodder command
        Command buyFodder = () -> warehouse.storedFodder.add(new Fodder(100));
        addCommand("BuyFodder", buyFodder);
    }


    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

   // public void getProducts

    public void feedAnimals() {
        for (Animal animal : animals) {
            if (warehouse.hasFodder()) {
                Fodder fodder = new Fodder(1);
                animal.eat(fodder);
                warehouse.takeFodder(fodder);
            }
            else {
                System.out.println("There is no fodder!");
                break;
            }
        }
    }

    public void starveAnimals(){
        List<Animal> toDel = new ArrayList<>();
        for (Animal animal : animals) {
            try {
                animal.starve();
            }
            catch (StarveToDeathException e) {
                System.out.println(e.getMessage());
                toDel.add(animal);
            }
        }

        for (Animal animal : toDel) {
            animals.remove(animal);
        }
    }

    public void addCommand(String cmd, Command action) {
        commands.put(cmd, action);
    }

    public void runCommand(String cmd) {
        try {
            commands.get(cmd).cmd();
        }
        catch (NullPointerException e) {
            System.out.println("No such command!");
        }
    }

    @Override
    public String toString() {
        String animalsString = "Animals: \n";
        for (Animal animal : animals) {
            animalsString += "\t" + animal + "\n";
        }
        return "Farm: \n" + warehouse + "\n" + animalsString;

    }
}
