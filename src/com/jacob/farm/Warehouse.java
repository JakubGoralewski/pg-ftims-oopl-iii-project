package com.jacob.farm;

import com.jacob.Products.Fodder;
import com.jacob.Products.Meat;
import com.jacob.Products.Milk;


public class Warehouse {
    Meat storedMeat = new Meat();
    Milk storedMilk = new Milk();
    Fodder storedFodder = new Fodder();

    public void addMilk(Milk milk) {
        storedMilk.add(milk);
    }

    public void addMeat(Meat meat) {
        storedMeat.add(meat);
    }

    public void addFodder(Fodder fodder) {
        storedFodder.add(fodder);
    }

    public boolean hasFodder() {
        return (storedFodder.getMass() > 0);
    }

    public void takeFodder(Fodder fodder) {
        storedFodder.take(fodder);
    }

    @Override
    public String toString() {
        return "Warehouse: " + storedMilk + "l, " + storedMeat + "kg, " + storedFodder;
    }
}
