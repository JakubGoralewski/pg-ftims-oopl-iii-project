package com.jacob;

import com.jacob.farm.Farm;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
	    Farm farm = new Farm();


        while(true) {
            System.out.println("===== New Day ======");
            System.out.println(farm);

            System.out.println("To see possible actions type \"Help\" and press enter.");
            String cmd = scanner.nextLine();
            farm.runCommand(cmd);

            farm.starveAnimals();
            System.out.println("===== End Day ======");
        }


    }

}
