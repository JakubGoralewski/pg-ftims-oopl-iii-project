package com.jacob.Products;


public class Fodder extends Food{
    public Fodder() {
        mass = 0;
    }

    public Fodder(int mass) {
        this.mass = mass;
    }

    public void add(Fodder fodder) {
        this.mass += fodder.mass;
    }

    public void take(Fodder fodder) {
        this.mass -= fodder.mass;
    }

    @Override
    public String toString() {
        return "Fodder: " + mass;
    }
}
