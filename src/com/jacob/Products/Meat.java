package com.jacob.Products;


public class Meat extends Food {

    public Meat() {
        mass = 0;
    }

    public Meat(int mass) {
        this.mass = mass;
    }

    public void add(Meat meat) {
        this.mass += meat.mass;
    }

    public void take(Meat meat) {
        this.mass -= meat.mass;
    }

    @Override
    public String toString() {
        return "Meat: " + mass;
    }
}
