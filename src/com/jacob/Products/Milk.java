package com.jacob.Products;


public class Milk extends Food {

    public Milk(){
        mass = 0;
    }

    public Milk(int mass) {
        this.mass = mass;
    }

    public void add(Milk milk) {
        this.mass += milk.mass;
    }

    public void take(Milk milk) {
        this.mass -= milk.mass;
    }

    @Override
    public String toString() {
        return "Milk: " + mass;
    }
}
